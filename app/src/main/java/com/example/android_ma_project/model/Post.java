package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("idPost")
    @Expose
    private Integer idPost;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("imagePath")
    @Expose
    private String imagePath;
    @SerializedName("idCommunity")
    @Expose
    private Integer idCommunity;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("idUser")
    @Expose
    private Integer idUser;
    @SerializedName("userName")
    @Expose
    private String userName;


    public Post(Integer idPost, String title, String text, String creationDate, String imagePath, Integer idCommunity, String name, Integer idUser, String userName) {
        this.idPost = idPost;
        this.title = title;
        this.text = text;
        this.creationDate = creationDate;
        this.imagePath = imagePath;
        this.idCommunity = idCommunity;
        this.name = name;
        this.idUser = idUser;
        this.userName = userName;
    }

    public Post(String title, String text, String imagePath, Integer idCommunity, Integer idUser) {
        this.title = title;
        this.text = text;
        this.imagePath = imagePath;
        this.idCommunity = idCommunity;
        this.idUser = idUser;
    }

    public Post(String title, String text, String imagePath) {
        this.title = title;
        this.text = text;
        this.imagePath = imagePath;
    }

    public Post() {

    }

    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getIdCommunity() {
        return idCommunity;
    }

    public void setIdCommunity(Integer idCommunity) {
        this.idCommunity = idCommunity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
