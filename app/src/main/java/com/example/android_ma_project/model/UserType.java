package com.example.android_ma_project.model;

public enum UserType {
    USER,
    ADMIN,
    MODERATOR
}
