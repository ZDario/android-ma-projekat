package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Admin {
    @SerializedName("idAdmin")
    @Expose
    private Integer idAdmin;
    @SerializedName("userNameAdmin")
    @Expose
    private String userNameAdmin;
    @SerializedName("passwordAdmin")
    @Expose
    private String passwordAdmin;
    @SerializedName("emailAdmin")
    @Expose
    private String emailAdmin;
    @SerializedName("avatarAdmin")
    @Expose
    private String avatarAdmin;
    @SerializedName("registrationDateAdmin")
    @Expose
    private String registrationDateAdmin;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("banned")
    @Expose
    private Boolean banned;

    public Admin(Integer idAdmin, String userNameAdmin, String passwordAdmin, String emailAdmin, String avatarAdmin, String registrationDateAdmin, String userType, Boolean banned) {
        this.idAdmin = idAdmin;
        this.userNameAdmin = userNameAdmin;
        this.passwordAdmin = passwordAdmin;
        this.emailAdmin = emailAdmin;
        this.avatarAdmin = avatarAdmin;
        this.registrationDateAdmin = registrationDateAdmin;
        this.userType = userType;
        this.banned = banned;
    }

    public Integer getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(Integer idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getUserNameAdmin() {
        return userNameAdmin;
    }

    public void setUserNameAdmin(String userNameAdmin) {
        this.userNameAdmin = userNameAdmin;
    }

    public String getPasswordAdmin() {
        return passwordAdmin;
    }

    public void setPasswordAdmin(String passwordAdmin) {
        this.passwordAdmin = passwordAdmin;
    }

    public String getEmailAdmin() {
        return emailAdmin;
    }

    public void setEmailAdmin(String emailAdmin) {
        this.emailAdmin = emailAdmin;
    }

    public String getAvatarAdmin() {
        return avatarAdmin;
    }

    public void setAvatarAdmin(String avatarAdmin) {
        this.avatarAdmin = avatarAdmin;
    }

    public String getRegistrationDateAdmin() {
        return registrationDateAdmin;
    }

    public void setRegistrationDateAdmin(String registrationDateAdmin) {
        this.registrationDateAdmin = registrationDateAdmin;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

}
