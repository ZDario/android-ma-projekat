package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reaction {

    @SerializedName("idReaction")
    @Expose
    private Integer idReaction;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("reactionType")
    @Expose
    private String reactionType;
    @SerializedName("idPost")
    @Expose
    private Integer idPost;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("idUser")
    @Expose
    private Integer idUser;
    @SerializedName("userName")
    @Expose
    private String userName;

    public Reaction(Integer idReaction, String timeStamp, String reactionType, Integer idUser, String userName, Integer idPost, String title) {
        this.idReaction = idReaction;
        this.timeStamp = timeStamp;
        this.reactionType = reactionType;
        this.idUser = idUser;
        this.userName = userName;
        this.idPost = idPost;
        this.title = title;
    }

    public Reaction(String reactionType, Integer idPost, Integer idUser) {
        this.reactionType = reactionType;
        this.idPost = idPost;
        this.idUser = idUser;
    }

    public Reaction(String reactionType) {
        this.reactionType = reactionType;
    }

    public Reaction(){

    }

    public Integer getIdReaction() {
        return idReaction;
    }

    public void setIdReaction(Integer idReaction) {
        this.idReaction = idReaction;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getReactionType() {
        return reactionType;
    }

    public void setReactionType(String reactionType) {
        this.reactionType = reactionType;
    }

    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
