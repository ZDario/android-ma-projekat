package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Moderator{

    @SerializedName("idModerator")
    @Expose
    private Integer idModerator;
    @SerializedName("userNameModerator")
    @Expose
    private String userNameModerator;
    @SerializedName("passwordModerator")
    @Expose
    private String passwordModerator;
    @SerializedName("emailModerator")
    @Expose
    private String emailModerator;
    @SerializedName("avatarModerator")
    @Expose
    private String avatarModerator;
    @SerializedName("registrationDateModerator")
    @Expose
    private String registrationDateModerator;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("idCommunity")
    @Expose
    private Integer idCommunity;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("banned")
    @Expose
    private Boolean banned;

    public Moderator(Integer idModerator, String userNameModerator, String passwordModerator, String emailModerator, String avatarModerator, String registrationDateModerator, String userType, Integer idCommunity, String name, Boolean banned) {
        this.idModerator = idModerator;
        this.userNameModerator = userNameModerator;
        this.passwordModerator = passwordModerator;
        this.emailModerator = emailModerator;
        this.avatarModerator = avatarModerator;
        this.registrationDateModerator = registrationDateModerator;
        this.userType = userType;
        this.idCommunity = idCommunity;
        this.name = name;
        this.banned = banned;
    }

    public Integer getIdModerator() {
        return idModerator;
    }

    public void setIdModerator(Integer idModerator) {
        this.idModerator = idModerator;
    }

    public String getUserNameModerator() {
        return userNameModerator;
    }

    public void setUserNameModerator(String userNameModerator) {
        this.userNameModerator = userNameModerator;
    }

    public String getPasswordModerator() {
        return passwordModerator;
    }

    public void setPasswordModerator(String passwordModerator) {
        this.passwordModerator = passwordModerator;
    }

    public String getEmailModerator() {
        return emailModerator;
    }

    public void setEmailModerator(String emailModerator) {
        this.emailModerator = emailModerator;
    }

    public String getAvatarModerator() {
        return avatarModerator;
    }

    public void setAvatarModerator(String avatarModerator) {
        this.avatarModerator = avatarModerator;
    }

    public String getRegistrationDateModerator() {
        return registrationDateModerator;
    }

    public void setRegistrationDateModerator(String registrationDateModerator) {
        this.registrationDateModerator = registrationDateModerator;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Integer getIdCommunity() {
        return idCommunity;
    }

    public void setIdCommunity(Integer idCommunity) {
        this.idCommunity = idCommunity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

}