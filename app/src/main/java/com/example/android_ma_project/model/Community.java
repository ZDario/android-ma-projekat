package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Community {

    @SerializedName("idCommunity")
    @Expose
    private Integer idCommunity;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("rules")
    @Expose
    private String rules;
    @SerializedName("isSuspended")
    @Expose
    private Boolean isSuspended;
    @SerializedName("suspendedReason")
    @Expose
    private String suspendedReason;


    public Community(Integer idCommunity, String name, String description, String creationDate, String rules, boolean isSuspended, String suspendedReason) {
        this.idCommunity = idCommunity;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
        this.rules = rules;
        this.isSuspended = isSuspended;
        this.suspendedReason = suspendedReason;
    }

    //ADD
    public Community(String name, String description, String rules) {
        this.name = name;
        this.description = description;
        this.rules = rules;
    }

    //UPDATE
    public Community(String description, String rules) {
        this.description = description;
        this.rules = rules;
    }

    //SUSPEND
    public Community(boolean isSuspended, String suspendedReason) {
        this.isSuspended = isSuspended;
        this.suspendedReason = suspendedReason;
    }

    public Community() {

    }

    public Integer getIdCommunity() {
        return idCommunity;
    }

    public void setIdCommunity(Integer idCommunity) {
        this.idCommunity = idCommunity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getSuspendedReason() {
        return suspendedReason;
    }

    public void setSuspendedReason(String suspendedReason) {
        this.suspendedReason = suspendedReason;
    }

    public Boolean getSuspended() {
        return isSuspended;
    }

    public void setSuspended(boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

}
