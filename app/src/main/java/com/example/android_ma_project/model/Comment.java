package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("idComment")
    @Expose
    private Integer idComment;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("idPost")
    @Expose
    private Integer idPost;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("idUser")
    @Expose
    private Integer idUser;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;

    public Comment(){

    }

    public Comment(Integer idComment, String timeStamp, String text, Boolean isDeleted, Integer idUser, String userName, Integer idPost, String title) {
        this.idComment = idComment;
        this.timeStamp = timeStamp;
        this.text = text;
        this.isDeleted = isDeleted;
        this.idUser = idUser;
        this.userName = userName;
        this.idPost = idPost;
        this.title = title;
    }

    public Comment(String text, Integer idPost, Integer idUser){
        this.text = text;
        this.idUser = idUser;
        this.idPost = idPost;
    }

    public Comment(String text, Boolean isDeleted){
        this.text = text;
        this.isDeleted = isDeleted;
    }

    public Comment(boolean isDeleted){
        this.isDeleted = isDeleted;
    }

    public Integer getIdComment() {
        return idComment;
    }

    public void setIdComment(Integer idComment) {
        this.idComment = idComment;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
