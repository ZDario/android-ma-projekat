package com.example.android_ma_project.model;

public enum ReactionType {
    UPVOTE,
    DOWNVOTE
}
