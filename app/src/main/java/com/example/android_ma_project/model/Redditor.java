package com.example.android_ma_project.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Redditor {

    @SerializedName("idRedditor")
    @Expose
    private Integer idRedditor;
    @SerializedName("userNameRedditor")
    @Expose
    private String userNameRedditor;
    @SerializedName("passwordRedditor")
    @Expose
    private String passwordRedditor;
    @SerializedName("emailRedditor")
    @Expose
    private String emailRedditor;
    @SerializedName("avatarRedditor")
    @Expose
    private String avatarRedditor;
    @SerializedName("registrationDateRedditor")
    @Expose
    private String registrationDateRedditor;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("banned")
    @Expose
    private Boolean banned;

    public Redditor(Integer idRedditor, String userNameRedditor, String passwordRedditor, String emailRedditor, String avatarRedditor, String registrationDateRedditor, String userType, Boolean banned) {
        this.idRedditor = idRedditor;
        this.userNameRedditor = userNameRedditor;
        this.passwordRedditor = passwordRedditor;
        this.emailRedditor = emailRedditor;
        this.avatarRedditor = avatarRedditor;
        this.registrationDateRedditor = registrationDateRedditor;
        this.userType = userType;
        this.banned = banned;
    }

    public Redditor(String userNameRedditor, String passwordRedditor, String emailRedditor, String avatarRedditor){
        this.userNameRedditor = userNameRedditor;
        this.passwordRedditor = passwordRedditor;
        this.emailRedditor = emailRedditor;
        this.avatarRedditor = avatarRedditor;
    }

    public Redditor(){

    }

    public Integer getIdRedditor() {
        return idRedditor;
    }

    public void setIdRedditor(Integer idRedditor) {
        this.idRedditor = idRedditor;
    }

    public String getUserNameRedditor() {
        return userNameRedditor;
    }

    public void setUserNameRedditor(String userNameRedditor) {
        this.userNameRedditor = userNameRedditor;
    }

    public String getPasswordRedditor() {
        return passwordRedditor;
    }

    public void setPasswordRedditor(String passwordRedditor) {
        this.passwordRedditor = passwordRedditor;
    }

    public String getEmailRedditor() {
        return emailRedditor;
    }

    public void setEmailRedditor(String emailRedditor) {
        this.emailRedditor = emailRedditor;
    }

    public String getAvatarRedditor() {
        return avatarRedditor;
    }

    public void setAvatarRedditor(String avatarRedditor) {
        this.avatarRedditor = avatarRedditor;
    }

    public String getRegistrationDateRedditor() {
        return registrationDateRedditor;
    }

    public void setRegistrationDateRedditor(String registrationDateRedditor) {
        this.registrationDateRedditor = registrationDateRedditor;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

}
