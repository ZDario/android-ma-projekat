package com.example.android_ma_project.service;

import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.model.Reaction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PostServiceInterface {

    @GET("post")
    Call<List<Post>> getPosts();

    @GET("post/{id}")
    Call<Post> getPost(@Path("id") int id);

    @POST("post")
    Call<Post> addPost(@Body Post post);

    @Headers({"Content-Type: application/json"})
    @PUT("post/{id}")
    Call<Post> editPost(@Body Post post, @Path("id") int id);

    @DELETE("post/{id}")
    Call<Void> deletePost(@Path("id") int id);

    @GET("post/{id}/comments")
    Call<List<Comment>> getCommentsOfPost(@Path("id") int id);

    @GET("post/{id}/reactions")
    Call<List<Reaction>> getReactionsOfPost(@Path("id") int id);
}
