package com.example.android_ma_project.service;

import com.example.android_ma_project.model.Reaction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ReactionServiceInterface {

    @GET("reaction")
    Call<List<Reaction>> getReactions();

    @GET("reaction/{id}")
    Call<Reaction> getReaction(@Path("id") int id);

    @POST("reaction")
    Call<Reaction> addReaction(@Body Reaction reaction);

    @Headers({"Content-Type: application/json"})
    @PUT("reaction/{id}")
    Call<Reaction> editReaction(@Body Reaction reaction, @Path("id") int id);

    @DELETE("reaction/{id}")
    Call<Void> deleteReaction(@Path("id") int id);
}
