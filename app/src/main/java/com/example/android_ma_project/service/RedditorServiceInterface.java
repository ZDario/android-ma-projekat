package com.example.android_ma_project.service;

import com.example.android_ma_project.model.Redditor;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RedditorServiceInterface {

    @GET("redditor")
    Call<List<Redditor>> getRedditors();

    @GET("redditor/{id}")
    Call<Redditor> getRedditor(@Path("id") int id);

    @POST("redditor")
    Call<Redditor> addRedditor(@Body Redditor redditor);

    @Headers({"Content-Type: application/json"})
    @PUT("redditor/{id}")
    Call<Redditor> editRedditor(@Body Redditor redditor, @Path("id") int id);

    @DELETE("redditor/{id}")
    Call<Void> deleteRedditor(@Path("id") int id);

    @POST("redditor/{id}/block")
    Call<Redditor> blockRedditor(@Path("id") int id);
}
