package com.example.android_ma_project.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public interface MyRetrofit {

    String BASE_URL = "http://192.168.251.155:8080/api/";

    //String BASE_URL = "http://192.168.0.219:8080/api/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}