package com.example.android_ma_project.service;


import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CommentServiceInterface {

    @GET("comment")
    Call<List<Comment>> getComments();

    @GET("comment/{id}")
    Call<Comment> getComment(@Path("id") int id);

    @POST("comment")
    Call<Comment> addComment(@Body Comment comment);

    @Headers({"Content-Type: application/json"})
    @PUT("comment/{id}")
    Call<Comment> editComment(@Body Comment comment, @Path("id") int id);

    @DELETE("comment/{id}")
    Call<Void> deleteComment(@Path("id") int id);

    @Headers({"Content-Type: application/json"})
    @PUT("comment/{id}/delete")
    Call<Comment> commentDelete(@Body Comment comment, @Path("id") int id);
}
