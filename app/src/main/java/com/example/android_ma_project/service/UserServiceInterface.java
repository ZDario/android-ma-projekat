package com.example.android_ma_project.service;

import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserServiceInterface {

    @GET("user")
    Call<List<User>> getUsers();

    @GET("user/{id}")
    Call<User> getUser(@Path("id") int id);

    @POST("user")
    Call<User> addUsers(@Body User user);

    @Headers({"Content-Type: application/json"})
    @PUT("user/{id}")
    Call<User> editUser(@Body User user, @Path("id") int id);

    @DELETE("user/{id}")
    Call<Void> deleteUser(@Path("id") int id);

    @POST("user/{id}/block")
    Call<User> blockUser(@Path("id") int id);

    @POST("user/login")
    Call<User> login(@Body User user);
}
