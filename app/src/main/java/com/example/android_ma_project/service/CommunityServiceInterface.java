package com.example.android_ma_project.service;

import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CommunityServiceInterface {

    @GET("community")
    Call<List<Community>> getCommunities();

    @GET("community/{id}")
    Call<Community> getCommunity(@Path("id") int id);

    @POST("community")
    Call<Community> addCommunity(@Body Community community);

    @Headers({"Content-Type: application/json"})
    @PUT("community/{id}")
    Call<Community> editCommunity(@Body Community community, @Path("id") int id);

    @DELETE("community/{id}")
    Call<Void> deleteCommunity(@Path("id") int id);

    @GET("community/{id}/posts")
    Call<List<Post>> getPostsOfCommunity(@Path("id") int id);

    @Headers({"Content-Type: application/json"})
    @PUT("community/{id}/suspend")
    Call<Community> suspendCommunity(@Body Community community, @Path("id") int id);
}
