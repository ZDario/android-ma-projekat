//package com.example.android_ma_project.dao;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;
//
//import com.example.android_ma_project.model.Community;
//import com.example.android_ma_project.model.Post;
//import com.example.android_ma_project.model.User;
//import com.example.android_ma_project.model.UserType;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//
//import static android.content.ContentValues.TAG;
//
//public class DatabaseHelper extends SQLiteOpenHelper {
//
//    private static final int DATABASE_VERSION = 1;
//    private static final String DATABASE_NAME = "reddit.db";
//
//    private static final String TABLE_USERS = "users";
//    private static final String TABLE_POSTS = "posts";
//    private static final String TABLE_COMMUNITY = "community";
//
//    private static final String KEY_ID = "idUser";
//    private static final String KEY_USERNAME = "userName";
//    private static final String KEY_PASSWORD = "password";
//    private static final String KEY_EMAIL = "email";
//    private static final String KEY_AVATAR = "avatar";
//    private static final String KEY_REGISTRATION_DATE = "registrationDate";
//    private static final String KEY_IS_BANNED = "isBanned";
//    private static final String KEY_USERTYPE = "userType";
//
//    private static final String KEY_ID_POST = "idPost";
//    private static final String KEY_TITLE = "title";
//    private static final String KEY_TEXT = "text";
//    private static final String KEY_CREATION_DATE = "creationDate";
//    private static final String KEY_IMAGE_PATH = "imagePath";
//    private static final String KEY_ID_COMMUNITY = "idCommunity";
//    private static final String KEY_ID_USERNAME = "idUser";
//    private static final String KEY_UP_VOTE = "upVote";
//    private static final String KEY_DOWN_VOTE = "downVote";
//    private static final String KEY_KARMA = "karma";
//
//    private static final String KEY_NAME = "name";
//    private static final String KEY_DESCRIPTION= "description";
//    private static final String KEY_RULES = "rules";
//    private static final String KEY_IS_SUSPENDED= "isSuspended";
//    private static final String KEY_SUSPENDED_REASON = "suspendedReason";
//
//
//
//    //https://guides.codepath.com/android/local-databases-with-sqliteopenhelper
//
//
//    public DatabaseHelper(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        //3rd argument to be passed is CursorFactory instance
//    }
//
//    @Override
//    public void onConfigure(SQLiteDatabase db) {
//        super.onConfigure(db);
//        db.setForeignKeyConstraintsEnabled(true);
//    }
//
//    // Creating Tables
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
//                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USERNAME + " TEXT," + KEY_PASSWORD + " TEXT,"
//                + KEY_EMAIL + " TEXT," + KEY_AVATAR + " TEXT," + KEY_REGISTRATION_DATE + " DATE,"
//                + KEY_IS_BANNED + " BOOLEAN," + KEY_USERTYPE + " TEXT" + ")";
//
//        String CREATE_COMMUNITY_TABLE = "CREATE TABLE " + TABLE_COMMUNITY + "("
//                + KEY_ID_COMMUNITY + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_DESCRIPTION + " TEXT,"
//                + KEY_CREATION_DATE + " DATE," + KEY_RULES + " TEXT," + KEY_IS_SUSPENDED + " BOOLEAN,"
//                + KEY_SUSPENDED_REASON + " TEXT" + ")";
//
//        String CREATE_POSTS_TABLE = "CREATE TABLE " + TABLE_POSTS + "("
//                + KEY_ID_POST + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT," + KEY_TEXT + " TEXT,"
//                + KEY_CREATION_DATE + " DATE," + KEY_IMAGE_PATH + " TEXT," + KEY_ID_COMMUNITY + " INTEGER REFERENCES " + TABLE_COMMUNITY+ ","
//                + KEY_ID_USERNAME + " INTEGER REFERENCES " + TABLE_USERS+ "," + KEY_UP_VOTE + " INTEGER,"
//                + KEY_DOWN_VOTE + " INTEGER," + KEY_KARMA + " INTEGER " +")";
//
//        db.execSQL(CREATE_COMMUNITY_TABLE);
//        db.execSQL(CREATE_USERS_TABLE);
//        db.execSQL(CREATE_POSTS_TABLE);
//    }
//
//    // Upgrading database
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        if (oldVersion != newVersion) {
//            // Drop older table if existed
//            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
//            db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTS);
//            db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMUNITY);
//            // Create tables again
//            onCreate(db);
//        }
//    }
//
//
//
//    //USER\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//
//    // ADD USER
//    public void addUser(User user) throws ParseException {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date parsed = sqlDateFormat.parse(user.getRegistrationDate());
//        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID, user.getIdUser());
//        values.put(KEY_USERNAME, user.getUserName());
//        values.put(KEY_PASSWORD, user.getPassword());
//        values.put(KEY_EMAIL, user.getEmail());
//        values.put(KEY_AVATAR, user.getAvatar());
//        values.put(KEY_REGISTRATION_DATE, sqlDate.toString());
//        values.put(KEY_IS_BANNED, user.isBanned());
//        values.put(KEY_USERTYPE, user.getUserType().toString());
//
//        // Inserting Row
//        db.insert(TABLE_USERS, null, values);
//        //2nd argument is String containing nullColumnHack
//        db.close(); // Closing database connection
//    }
//
//    // UPDATE SINGLE USER
//    public int updateUser(User user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID, user.getIdUser());
//        values.put(KEY_USERNAME, user.getUserName());
//        values.put(KEY_PASSWORD, user.getPassword());
//        values.put(KEY_EMAIL, user.getEmail());
//        values.put(KEY_AVATAR, user.getAvatar());
//        values.put(KEY_REGISTRATION_DATE, user.getRegistrationDate());
//        values.put(KEY_IS_BANNED, user.isBanned());
//        values.put(KEY_USERTYPE, user.getUserType().toString());
//
//        // updating row
//        return db.update(TABLE_USERS, values, KEY_ID + " = ?",
//                new String[] { String.valueOf(user.getIdUser()) });
//    }
//
//    // code to get all users in a list view
//    public ArrayList<User> getAllUsers() {
//        ArrayList<User> userList = new ArrayList<User>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_USERS;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                User user = new User();
//                user.setIdUser(Integer.parseInt(cursor.getString(0)));
//                user.setUserName(cursor.getString(1));
//                user.setPassword(cursor.getString(2));
//                user.setEmail(cursor.getString(3));
//                user.setAvatar(cursor.getString(4));
//                user.setRegistrationDate(cursor.getString(5));
//                user.setBanned(Boolean.parseBoolean(cursor.getString(6)));
//                user.setUserType(UserType.valueOf(cursor.getString(7)));
//                // Adding post to list
//                userList.add(user);
//            } while (cursor.moveToNext());
//        }
//        return userList;
//    }
//
//    // GET USER
//    User getUser(int idUser) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_USERS, new String[] { KEY_ID, KEY_USERNAME, KEY_PASSWORD, KEY_EMAIL, KEY_AVATAR, KEY_REGISTRATION_DATE, KEY_IS_BANNED, KEY_USERTYPE }, KEY_ID + "=?",
//                new String[] { String.valueOf(idUser) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        User user = new User(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2),
//                cursor.getString(3), cursor.getString(4),
//                cursor.getString(5), Boolean.parseBoolean(cursor.getString(6)),
//                UserType.valueOf(cursor.getString(7)));
//        return user;
//    }
//
//    // CLEAR TABLE USER
//    public void clearUserTable(){
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DELETE FROM " + TABLE_USERS);
//        db.close();
//    }
//
//    // DELETE SINGLE USER
//    public void deleteUser(User user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_USERS, KEY_ID + " = ?",
//                new String[] { String.valueOf(user.getIdUser()) });
//        db.close();
//    }
//
//    // GET USER COUNT
//    public int getUsersCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_USERS;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        int count = cursor.getCount();
//        cursor.close();
//
//        // return count
//        return count;
//    }
//
//    //CHECK USERNAME AND PASSWORD ON LOGIN
//    public Boolean checkUserNameUser(String userName) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS + " WHERE userName = ?", new String[]{userName});
//        if (cursor.getCount()>0){
//            return  true;
//        }else{
//            return false;
//        }
//    }
//    public Boolean checkUserNameAndPasswordUser(String userName, String password, String userType) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS + "  WHERE userName = ? AND password = ? AND userType = ?", new String[]{userName, password, userType});
//        if (cursor.getCount()>0){
//            return  true;
//        }else{
//            return false;
//        }
//    }
//
//    //https://www.tabnine.com/code/java/methods/android.database.sqlite.SQLiteDatabase/rawQuery
//
//
//
//
//    //POSTS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//
//    // code to get the single post
//    public Post getPost(int idPost) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_POSTS, new String[] { KEY_ID_POST, KEY_TITLE, KEY_TEXT, KEY_CREATION_DATE,
//                        KEY_IMAGE_PATH, KEY_ID_COMMUNITY, KEY_ID_USERNAME, KEY_UP_VOTE, KEY_DOWN_VOTE, KEY_KARMA }, KEY_ID_POST + "=?",
//                new String[] { String.valueOf(idPost) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        Post post = new Post(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2),
//                cursor.getString(3), cursor.getString(4),
//                Integer.parseInt(cursor.getString(5)),Integer.parseInt(cursor.getString(6)),
//                Integer.parseInt(cursor.getString(7)),Integer.parseInt(cursor.getString(8)),
//                Integer.parseInt(cursor.getString(9)));
//        return post;
//    }
//
//    // code to get all posts in a list view
//    public ArrayList<Post> getAllPostsAsc() {
//        ArrayList<Post> postList = new ArrayList<Post>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_POSTS + " ORDER " + " BY " + KEY_KARMA + " ASC";
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Post post = new Post();
//                post.setIdPost(Integer.parseInt(cursor.getString(0)));
//                post.setTitle(cursor.getString(1));
//                post.setText(cursor.getString(2));
//                post.setCreationDate(cursor.getString(3));
//                post.setImagePath(cursor.getString(4));
//                post.setIdCommunity(Integer.parseInt(cursor.getString(5)));
//                post.setIdUser(Integer.parseInt(cursor.getString(6)));
//                post.setUpVote(Integer.parseInt(cursor.getString(7)));
//                post.setDownVote(Integer.parseInt(cursor.getString(8)));
//                post.setKarma(Integer.parseInt(cursor.getString(9)));
//                // Adding post to list
//                postList.add(post);
//            } while (cursor.moveToNext());
//        }
//        return postList;
//    }
//
//
//    // code to get all posts in a list view
//    public ArrayList<Post> getAllPostsDesc() {
//        ArrayList<Post> postList = new ArrayList<Post>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_POSTS + " ORDER " + " BY " + KEY_KARMA + " DESC";
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Post post = new Post();
//                post.setIdPost(Integer.parseInt(cursor.getString(0)));
//                post.setTitle(cursor.getString(1));
//                post.setText(cursor.getString(2));
//                post.setCreationDate(cursor.getString(3));
//                post.setImagePath(cursor.getString(4));
//                post.setIdCommunity(Integer.parseInt(cursor.getString(5)));
//                post.setIdUser(Integer.parseInt(cursor.getString(6)));
//                post.setUpVote(Integer.parseInt(cursor.getString(7)));
//                post.setDownVote(Integer.parseInt(cursor.getString(8)));
//                post.setKarma(Integer.parseInt(cursor.getString(9)));
//                // Adding post to list
//                postList.add(post);
//            } while (cursor.moveToNext());
//        }
//        return postList;
//    }
//
//    // code to get all posts in a list view for community id
//    public ArrayList<Post> getAllPostsForCommunity(Integer idCommunity) {
//        ArrayList<Post> postList = new ArrayList<Post>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_POSTS + " WHERE " + KEY_ID_COMMUNITY + " = " + idCommunity;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Post post = new Post();
//                post.setIdPost(Integer.parseInt(cursor.getString(0)));
//                post.setTitle(cursor.getString(1));
//                post.setText(cursor.getString(2));
//                post.setCreationDate(cursor.getString(3));
//                post.setImagePath(cursor.getString(4));
//                post.setIdCommunity(Integer.parseInt(cursor.getString(5)));
//                post.setIdUser(Integer.parseInt(cursor.getString(6)));
//                post.setUpVote(Integer.parseInt(cursor.getString(7)));
//                post.setDownVote(Integer.parseInt(cursor.getString(8)));
//                post.setKarma(Integer.parseInt(cursor.getString(9)));
//                // Adding post to list
//                postList.add(post);
//            } while (cursor.moveToNext());
//        }
//        return postList;
//    }
//
//    //code to add the new post
//    public void addPost(Post post) throws ParseException {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date parsed = sqlDateFormat.parse(post.getCreationDate());
//        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID_POST, post.getIdPost());
//        values.put(KEY_TITLE, post.getTitle());
//        values.put(KEY_TEXT, post.getText());
//        values.put(KEY_CREATION_DATE, sqlDate.toString());
//        values.put(KEY_IMAGE_PATH, post.getImagePath());
//        values.put(KEY_ID_COMMUNITY, post.getIdCommunity());
//        values.put(KEY_ID_USERNAME, post.getIdUser());
//        values.put(KEY_UP_VOTE, post.getUpVote());
//        values.put(KEY_DOWN_VOTE, post.getDownVote());
//        values.put(KEY_KARMA, post.getKarma());
//
//        // Inserting Row
//        db.insert(TABLE_POSTS, null, values);
//        //2nd argument is String containing nullColumnHack
//        db.close(); // Closing database connection
//    }
//
//    // code to update the single post
//    public int updatePost(Post post) throws ParseException {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date parsed = sqlDateFormat.parse(post.getCreationDate());
//        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID_POST, post.getIdPost());
//        values.put(KEY_TITLE, post.getTitle());
//        values.put(KEY_TEXT, post.getText());
//        values.put(KEY_CREATION_DATE, sqlDate.toString());
//        values.put(KEY_IMAGE_PATH, post.getImagePath());
//        values.put(KEY_ID_COMMUNITY, post.getIdCommunity());
//        values.put(KEY_ID_USERNAME, post.getIdUser());
//        values.put(KEY_UP_VOTE, post.getUpVote());
//        values.put(KEY_DOWN_VOTE, post.getDownVote());
//        values.put(KEY_KARMA, post.getKarma());
//
//        // updating row
//        return db.update(TABLE_POSTS, values, KEY_ID_POST + " = ?",
//                new String[] { String.valueOf(post.getIdPost()) });
//    }
//
//    // Deleting single post
//    public void deletePost(Post post) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_POSTS, KEY_ID_POST + " = ?",
//                new String[] { String.valueOf(post.getIdPost()) });
//        db.close();
//    }
//
//    // GET POST COUNT
//    public int getPostsCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_POSTS;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        int count = cursor.getCount();
//        cursor.close();
//
//        // return count
//        return count;
//    }
//
//    public void clearPostTable(){
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DELETE FROM " + TABLE_POSTS);
//        db.close();
//    }
//
//    public Boolean checkIdPost(Integer idPost) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_POSTS + " WHERE idPost = ?", new String[]{idPost.toString()});
//        if (cursor.getCount()>0){
//            return  true;
//        }else{
//            return false;
//        }
//    }
//
//
//
//    //COMMUNITY\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//
//    public Community getCommunity(int idCommunity) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_COMMUNITY, new String[] { KEY_ID_COMMUNITY, KEY_NAME, KEY_DESCRIPTION, KEY_CREATION_DATE,
//                        KEY_RULES, KEY_IS_SUSPENDED, KEY_SUSPENDED_REASON }, KEY_ID_COMMUNITY + "=?",
//                new String[] { String.valueOf(idCommunity) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        Community community = new Community(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2),
//                cursor.getString(3), cursor.getString(4),
//                Boolean.parseBoolean(cursor.getString(5)),cursor.getString(6));
//        return community;
//    }
//
//    // code to get all communities in a list view
//    public ArrayList<Community> getAllCommunities() {
//        ArrayList<Community> communityList = new ArrayList<Community>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_COMMUNITY;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Community community = new Community();
//                community.setIdCommunity(Integer.parseInt(cursor.getString(0)));
//                community.setName(cursor.getString(1));
//                community.setDescription(cursor.getString(2));
//                community.setCreationDate(cursor.getString(3));
//                community.setRules(cursor.getString(4));
//                community.setSuspended(Boolean.parseBoolean(cursor.getString(5)));
//                community.setSuspendedReason(cursor.getString(6));
//                // Adding community to list
//                communityList.add(community);
//            } while (cursor.moveToNext());
//        }
//        return communityList;
//    }
//
//
//    //code to add the new community
//    public void addCommunity(Community community) throws ParseException {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date parsed = sqlDateFormat.parse(community.getCreationDate());
//        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID_COMMUNITY, community.getIdCommunity());
//        values.put(KEY_NAME, community.getName());
//        values.put(KEY_DESCRIPTION, community.getDescription());
//        values.put(KEY_CREATION_DATE, sqlDate.toString());
//        values.put(KEY_RULES, community.getRules());
//        values.put(KEY_IS_SUSPENDED, community.isSuspended());
//        values.put(KEY_SUSPENDED_REASON, community.getSuspendedReason());
//
//        // Inserting Row
//        db.insert(TABLE_COMMUNITY, null, values);
//        //2nd argument is String containing nullColumnHack
//        db.close(); // Closing database connection
//    }
//
//    // code to update the single community
//    public int updateCommunity(Community community) throws ParseException {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date parsed = sqlDateFormat.parse(community.getCreationDate());
//        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID_COMMUNITY, community.getIdCommunity());
//        values.put(KEY_NAME, community.getName());
//        values.put(KEY_DESCRIPTION, community.getDescription());
//        values.put(KEY_CREATION_DATE, sqlDate.toString());
//        values.put(KEY_RULES, community.getRules());
//        values.put(KEY_IS_SUSPENDED, community.isSuspended());
//        values.put(KEY_SUSPENDED_REASON, community.getSuspendedReason());
//
//        // updating row
//        return db.update(TABLE_COMMUNITY, values, KEY_ID_COMMUNITY + " = ?",
//                new String[] { String.valueOf(community.getIdCommunity()) });
//    }
//
//    // Deleting single community
//    public void deleteCommunity(Community community) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_COMMUNITY, KEY_ID_COMMUNITY + " = ?",
//                new String[] { String.valueOf(community.getIdCommunity()) });
//        db.close();
//    }
//
//    public void clearCommunityTable(){
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DELETE FROM " + TABLE_COMMUNITY);
//        db.close();
//    }
//
//
//    // GET POST COUNT
//    public int getCommunitiesCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_COMMUNITY;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        int count = cursor.getCount();
//        cursor.close();
//
//        // return count
//        return count;
//    }
//
//    public Boolean checkIdCommunity(Integer idCommunity) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_COMMUNITY + " WHERE idCommunity = ?", new String[]{idCommunity.toString()});
//        if (cursor.getCount()>0){
//            return  true;
//        }else{
//            return false;
//        }
//    }
//
//}
