package com.example.android_ma_project.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.activities.CommentUpdateActivity;
import com.example.android_ma_project.activities.MenuUserActivity;
import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.model.Reaction;

import java.util.List;

public class UserReactionAdapter extends RecyclerView.Adapter<UserReactionAdapter.ViewHolder> {

    private List<Reaction> reactions;
    private Context context;

    public UserReactionAdapter(List<Reaction> data, Context context) {
        this.reactions = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView reactionType;
        TextView timeStamp;
        TextView idUser;
        TextView idPost;

        ViewHolder(View itemView) {
            super(itemView);
            reactionType = itemView.findViewById(R.id.reaction_reaction_type_single);
            timeStamp = itemView.findViewById(R.id.reaction_time_stamp_single);
            idUser = itemView.findViewById(R.id.reaction_id_user_single);
            idPost = itemView.findViewById(R.id.reaction_id_post_single);
        }
    }

    @Override
    public UserReactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reaction_single_list, parent, false);
        return new UserReactionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserReactionAdapter.ViewHolder holder, int position) {

        Reaction reaction = reactions.get(position);

        holder.reactionType.setText(reaction.getReactionType());
        holder.timeStamp.setText(reaction.getTimeStamp());
        holder.idUser.setText(reaction.getUserName());
        holder.idPost.setText(reaction.getTitle());

//        if(reaction.getReactionType() == "UPVOTE"){
//
//        }else if(reaction.getReactionType() == "DOWNVOTE"){
//
//        }
    }

    @Override
    public int getItemCount() {
        return reactions.size();
    }
}
