package com.example.android_ma_project.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.User;

import java.util.ArrayList;

public class UserAdapter extends BaseAdapter {

    Context context;
    ArrayList<User> arrayList;

    public UserAdapter(Context context, ArrayList<User> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return this.arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.user_single_list, null);
        }
        else {
            view = convertView;
        }

        TextView idView = (TextView) view.findViewById(R.id.user_id_single);
        TextView userNameView = (TextView) view.findViewById(R.id.user_userName_single);
        TextView passwordView = (TextView) view.findViewById(R.id.user_password_single);
        TextView emailView = (TextView) view.findViewById(R.id.user_email_single);
        ImageView avatarView = (ImageView) view.findViewById(R.id.user_avatar_single);
        TextView registrationDateView = (TextView) view.findViewById(R.id.user_registration_date_single);
        TextView isBannedView = (TextView) view.findViewById(R.id.user_is_banned_single);
        TextView userType = (TextView) view.findViewById(R.id.user_user_type_single);

        idView.setText(String.valueOf( arrayList.get(position).getIdUser()));
        userNameView.setText( arrayList.get(position).getUserName() );
        passwordView.setText( arrayList.get(position).getPassword() );
        emailView.setText( arrayList.get(position).getEmail());
        registrationDateView.setText(String.valueOf( arrayList.get(position).getRegistrationDate()));
        isBannedView.setText( String.valueOf(arrayList.get(position).isBanned()));
        userType.setText(String.valueOf( arrayList.get(position).getUserType()));

        byte[] decodedString = Base64.decode(arrayList.get(position).getAvatar(), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        avatarView.setImageBitmap(decodedByte);

        return view;
    }
}