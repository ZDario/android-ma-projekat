package com.example.android_ma_project.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.activities.MenuUserActivity;
import com.example.android_ma_project.activities.NotLoggedPostCommentListActivity;
import com.example.android_ma_project.activities.PostActivity;
import com.example.android_ma_project.activities.PostUpdateActivity;
import com.example.android_ma_project.activities.UserCommentListActivity;
import com.example.android_ma_project.activities.UserReactionListActivity;
import com.example.android_ma_project.model.Post;

import java.util.List;

public class UserPostAdapter extends RecyclerView.Adapter<UserPostAdapter.ViewHolder> {

    private List<Post> posts;
    private Context context;

    public UserPostAdapter(List<Post> data, Context context) {
        this.posts = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView id_community;
        TextView id_user;
        TextView title;
        TextView text;
        TextView creationDate;
        ImageView vote;
        ImageView reactions;

        ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.post_id);
            id_community = itemView.findViewById(R.id.post_id_community);
            id_user = itemView.findViewById(R.id.post_id_user);
            title = itemView.findViewById(R.id.post_title);
            text = itemView.findViewById(R.id.post_text);
            creationDate = itemView.findViewById(R.id.post_creation_date);
            vote = itemView.findViewById(R.id.vote);
            reactions = itemView.findViewById(R.id.reactions);
        }
    }

    @Override
    public UserPostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_single_list, parent, false);
        return new UserPostAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserPostAdapter.ViewHolder holder, int position) {

        Post post = posts.get(position);

        holder.id.setText(post.getIdPost().toString());
        holder.id_community.setText(post.getName());
        holder.id_user.setText(post.getUserName());
        holder.title.setText(post.getTitle());
        holder.text.setText(post.getText());
        holder.creationDate.setText(post.getCreationDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UserCommentListActivity.class);
                Bundle b = new Bundle();
                b.putInt("idPost", post.getIdPost());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(MenuUserActivity.idRedditor == post.getIdUser())
                {
                    Intent intent = new Intent(context, PostUpdateActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("idUser", post.getIdUser());
                    b.putInt("idPost", post.getIdPost());
                    intent.putExtras(b);
                    context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    return true;
                } else
                {
                    return false;
                }
            }
        });
        holder.vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PostActivity.class);
                Bundle b = new Bundle();
                b.putInt("idPost", post.getIdPost());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        holder.reactions.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UserReactionListActivity.class);
                Bundle b = new Bundle();
                b.putInt("idPost", post.getIdPost());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
}
