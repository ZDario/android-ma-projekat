package com.example.android_ma_project.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.activities.NotLoggedCommunityPostListActivity;
import com.example.android_ma_project.activities.NotLoggedPostCommentListActivity;
import com.example.android_ma_project.model.Post;

import java.util.List;

public class NotLoggedPostAdapter extends RecyclerView.Adapter<NotLoggedPostAdapter.ViewHolder> {

    private List<Post> posts;
    private Context context;

    public NotLoggedPostAdapter(List<Post> data, Context context) {
        this.posts = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView id_community;
        TextView id_user;
        TextView title;
        TextView text;
        TextView creationDate;

        ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.post_id);
            id_community = itemView.findViewById(R.id.post_id_community);
            id_user = itemView.findViewById(R.id.post_id_user);
            title = itemView.findViewById(R.id.post_title);
            text = itemView.findViewById(R.id.post_text);
            creationDate = itemView.findViewById(R.id.post_creation_date);
        }
    }

    @Override
    public NotLoggedPostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_single_list_not_logged, parent, false);
        return new NotLoggedPostAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotLoggedPostAdapter.ViewHolder holder, int position) {

        Post post = posts.get(position);

        holder.id.setText(post.getIdPost().toString());
        holder.id_community.setText(post.getName());
        holder.id_user.setText(post.getUserName());
        holder.title.setText(post.getTitle());
        holder.text.setText(post.getText());
        holder.creationDate.setText(post.getCreationDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NotLoggedPostCommentListActivity.class);
                Bundle b = new Bundle();
                b.putInt("idPost", post.getIdPost());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
}
