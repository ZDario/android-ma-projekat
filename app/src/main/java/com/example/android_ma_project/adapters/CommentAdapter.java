package com.example.android_ma_project.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.activities.CommentUpdateActivity;
import com.example.android_ma_project.model.Comment;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private List<Comment> comments;
    private Context context;

    public CommentAdapter(List<Comment> data, Context context) {
        this.comments = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView text;
        TextView timeStamp;
        TextView idUser;
        TextView idPost;

        ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.comment_id_single);
            text = itemView.findViewById(R.id.comment_text_single);
            timeStamp = itemView.findViewById(R.id.comment_time_stamp_single);
            idUser = itemView.findViewById(R.id.comment_user_single);
            idPost = itemView.findViewById(R.id.comment_post_single);
        }
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_single_list, parent, false);
        return new CommentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder holder, int position) {

        Comment comment = comments.get(position);

        holder.id.setText(comment.getIdComment().toString());
        holder.text.setText(comment.getText());
        holder.timeStamp.setText(comment.getTimeStamp());
        holder.idUser.setText(comment.getUserName());
        holder.idPost.setText(comment.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CommentUpdateActivity.class);
                Bundle b = new Bundle();
                b.putInt("idComment", comment.getIdComment());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }
}
