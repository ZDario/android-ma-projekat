package com.example.android_ma_project.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.activities.CommunityUpdateActivity;
import com.example.android_ma_project.activities.MenuUserActivity;
import com.example.android_ma_project.activities.NotLoggedCommunityPostListActivity;
import com.example.android_ma_project.activities.PostUpdateActivity;
import com.example.android_ma_project.activities.UserPostListActivity;
import com.example.android_ma_project.model.Community;

import java.util.List;

public class UserCommunityAdapter extends RecyclerView.Adapter<UserCommunityAdapter.ViewHolder> {

    private List<Community> communities;
    private Context context;

    public UserCommunityAdapter(List<Community> data, Context context) {
        this.communities = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView name;
        TextView description;
        TextView creationDate;

        ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.community_id);
            name = itemView.findViewById(R.id.community_name);
            description = itemView.findViewById(R.id.community_description);
            creationDate = itemView.findViewById(R.id.community_creation_date);
        }
    }

    @Override
    public UserCommunityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.community_single_list, parent, false);
        return new UserCommunityAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserCommunityAdapter.ViewHolder holder, int position) {

        Community community = communities.get(position);

        holder.id.setText(community.getIdCommunity().toString());
        holder.name.setText(community.getName());
        holder.description.setText(community.getDescription());
        holder.creationDate.setText(community.getCreationDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UserPostListActivity.class);
                Bundle b = new Bundle();
                b.putInt("idCommunity", community.getIdCommunity());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent intent = new Intent(context, CommunityUpdateActivity.class);
                Bundle b = new Bundle();
                b.putInt("idCommunity", community.getIdCommunity());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return communities.size();
    }
}
