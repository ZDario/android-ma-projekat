package com.example.android_ma_project.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.activities.CommunityActivity;
import com.example.android_ma_project.activities.NotLoggedPostCommentListActivity;
import com.example.android_ma_project.model.Post;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private List<Post> posts;
    private Context context;

    public PostAdapter(List<Post> data, Context context) {
        this.posts = data;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView id;
        TextView id_community;
        TextView id_user;
        TextView title;
        TextView text;
        TextView creationDate;

        ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.post_id);
            id_community = itemView.findViewById(R.id.post_id_community);
            id_user = itemView.findViewById(R.id.post_id_user);
            title = itemView.findViewById(R.id.post_title);
            text = itemView.findViewById(R.id.post_text);
            creationDate = itemView.findViewById(R.id.post_creation_date);
        }
    }

    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_single_list_not_logged, parent, false);
        return new PostAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostAdapter.ViewHolder holder, int position) {

        Post post = posts.get(position);

        holder.id.setText(post.getIdPost().toString());
        holder.id_community.setText(post.getName());
        holder.id_user.setText(post.getUserName());
        holder.title.setText(post.getTitle());
        holder.text.setText(post.getText());
        holder.creationDate.setText(post.getCreationDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CommunityActivity.class);
                Bundle b = new Bundle();
                b.putInt("idCommunity", post.getIdCommunity());
                intent.putExtras(b);
                context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }
}
