package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminSuspendCommunityActivity extends AppCompatActivity {

    private int idCommunity;
    private Boolean suspend=true;
    private TextView name_suspend, description_suspend, creation_date_suspend, rules_suspend;
    private EditText suspended_reason_suspend;
    private Button suspendCommunity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_suspend_community_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");

        name_suspend = findViewById(R.id.name_suspend);
        description_suspend = findViewById(R.id.description_suspend);
        creation_date_suspend = findViewById(R.id.creation_date_suspend);
        rules_suspend = findViewById(R.id.rules_suspend);
        suspended_reason_suspend = findViewById(R.id.suspended_reason_suspend);
        suspendCommunity = findViewById(R.id.suspendCommunity);

        suspendCommunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editCommunity();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        getCommunity();
    }

    private void getCommunity(){
        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<Community> call = service.getCommunity(idCommunity);
        call.enqueue(new Callback<Community>() {
            @Override
            public void onResponse(Call<Community> call, Response<Community> response) {

                Community community = response.body();
                name_suspend.setText(response.body().getName());
                description_suspend.setText(response.body().getDescription());
                creation_date_suspend.setText(response.body().getCreationDate());
                rules_suspend.setText(response.body().getRules());
                suspended_reason_suspend.setText(response.body().getSuspendedReason());
            }
            @Override
            public void onFailure(Call<Community> call, Throwable t) {
            }
        });
    }

    private void editCommunity() {
        String suspended_reason = suspended_reason_suspend.getText().toString().trim();

        if(suspended_reason.isEmpty()){
            suspended_reason_suspend.setError("Enter suspended reason");
            suspended_reason_suspend.requestFocus();
            return;
        }

        suspendUpdateCommunity(suspend, suspended_reason);
    }


    private void suspendUpdateCommunity(Boolean suspend, String suspend_reason) {

        Community community = new Community(suspend, suspend_reason);
        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);
        Call<Community> call = service.suspendCommunity(community, idCommunity);
        call.enqueue(new Callback<Community>() {
            @Override
            public void onResponse(Call<Community> call, Response<Community> response) {
                Community communityResponse = response.body();

                Boolean isSuspended1 = true;
                String suspendedReason = communityResponse.getSuspendedReason();

                community.setSuspended(isSuspended1);
                community.setSuspendedReason(suspendedReason);

                Toast.makeText(AdminSuspendCommunityActivity.this, "You have successfully suspended community", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AdminSuspendCommunityActivity.this, AdminCommunityListActivity.class);
                AdminSuspendCommunityActivity.this.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            @Override
            public void onFailure(Call<Community> call, Throwable t) {
            }
        });
    }
}
