package com.example.android_ma_project.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.RedditorServiceInterface;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileUpdateActivity extends Activity {

    private int idRedditor;
    private TextView user_userName_update, user_password_update, user_email_update, user_avatar_update;
    private Button updateUserProfileButton;
    private String sImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_update_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idRedditor = b.getInt("idRedditor");

        user_userName_update = findViewById(R.id.user_userName_update);
        user_password_update = findViewById(R.id.user_password_update);
        user_email_update = findViewById(R.id.user_email_update);
        user_avatar_update = findViewById(R.id.user_avatar_update);
        updateUserProfileButton = findViewById(R.id.updateUserProfileButton);


        updateUserProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUser();
            }
        });
        user_avatar_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check condition
                if (ContextCompat.checkSelfPermission(UserProfileUpdateActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {
                    // when permission is nor granted
                    // request permission
                    ActivityCompat.requestPermissions(UserProfileUpdateActivity.this
                            , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);

                }
                else
                {
                    // when permission
                    // is granted
                    // create method
                    selectImage();
                }
            }
        });
    }

    private void selectImage() {
        // clear previous data
        user_avatar_update.setText("");
        // Initialize intent
        Intent intent=new Intent(Intent.ACTION_PICK);
        // set type
        intent.setType("image/*");
        // start activity result
        startActivityForResult(Intent.createChooser(intent,"Select Image"),100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull  int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // check condition
        if (requestCode==100 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
        {
            // when permission
            // is granted
            // call method
            selectImage();
        }
        else
        {
            // when permission is denied
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check condition
        if (requestCode==100 && resultCode==RESULT_OK && data!=null)
        {
            // when result is ok
            // initialize uri
            Uri uri=data.getData();
            // Initialize bitmap
            try {
                Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                // initialize byte stream
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                // compress Bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
                // Initialize byte array
                byte[] bytes=stream.toByteArray();
                // get base64 encoded string
                sImage= Base64.encodeToString(bytes,Base64.DEFAULT);
                // set encoded text on textview
                user_avatar_update.setText(sImage);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getUserProfile();
    }

    private void getUserProfile(){
        RedditorServiceInterface service = MyRetrofit.retrofit.create(RedditorServiceInterface.class);

        Call<Redditor> call = service.getRedditor(idRedditor);
            call.enqueue(new Callback<Redditor>() {
            @Override
            public void onResponse(Call<Redditor> call, Response<Redditor> response) {

                Redditor redditor = response.body();

                user_userName_update.setText(response.body().getUserNameRedditor());
                user_password_update.setText(response.body().getPasswordRedditor());
                user_email_update.setText(response.body().getEmailRedditor());
                user_avatar_update.setText(sImage);
            }
            @Override
            public void onFailure(Call<Redditor> call, Throwable t) {
            }
        });
    }

    private void editUser() {
        String user_userName = user_userName_update.getText().toString().trim();
        String user_password = user_password_update.getText().toString().trim();
        String user_email = user_email_update.getText().toString().trim();
        String user_avatar = user_avatar_update.getText().toString().trim();

        if(user_userName.isEmpty()){
            user_userName_update.setError("Enter new user name");
            user_userName_update.requestFocus();
            return;
        }
        else if(user_password.isEmpty()){
            user_password_update.setError("Enter password again");
            user_password_update.requestFocus();
            return;
        }
        else if(user_email.isEmpty()){
            user_email_update.setError("Enter new email");
            user_email_update.requestFocus();
            return;
        }
        else if(user_avatar.isEmpty()){
            user_avatar_update.setError("Enter new avatar");
            user_avatar_update.requestFocus();
            return;
        }
        else if (!isValidEmail(user_email)) {
            user_email_update.setError("Invalid Email");
            return;
        }

        saveUpdatedUser(user_userName, user_password, user_email, user_avatar);
    }

    private void saveUpdatedUser(String user_userName, String user_password, String user_email, String user_avatar) {

        Redditor redditor = new Redditor(user_userName, user_password, user_email, user_avatar);

        RedditorServiceInterface service = MyRetrofit.retrofit.create(RedditorServiceInterface.class);

        Call<Redditor> call = service.editRedditor(redditor, idRedditor);
        call.enqueue(new Callback<Redditor>() {
            @Override
            public void onResponse(Call<Redditor> call, Response<Redditor> response) {

                Redditor redditorResponse = response.body();

                String userName = redditorResponse.getUserNameRedditor();
                String password = redditorResponse.getPasswordRedditor();
                String email = redditorResponse.getEmailRedditor();
                String avatar = redditorResponse.getAvatarRedditor();

                redditor.setUserNameRedditor(userName);
                redditor.setPasswordRedditor(password);
                redditor.setEmailRedditor(email);
                redditor.setAvatarRedditor(avatar);

                Toast.makeText(UserProfileUpdateActivity.this, "You have successfully updated your profile", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(UserProfileUpdateActivity.this, UserProfileActivity.class);
                UserProfileUpdateActivity.this.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

            }

            @Override
            public void onFailure(Call<Redditor> call, Throwable t) {
            }
        });
    }

    private Boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserProfileUpdateActivity.this, UserProfileActivity.class);
        startActivity(intent);
        finish();
    }
}
