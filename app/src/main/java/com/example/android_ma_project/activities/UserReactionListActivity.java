package com.example.android_ma_project.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.UserReactionAdapter;
import com.example.android_ma_project.model.Reaction;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserReactionListActivity extends AppCompatActivity {

    private int idPost;
    private Context context;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reaction_list_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idPost = b.getInt("idPost");
    }

    @Override
    public void onResume(){
        super.onResume();
        getReactionsOfPost();
    }

    private void getReactionsOfPost() {

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);

        Call<List<Reaction>> call = service.getReactionsOfPost(idPost);
        call.enqueue(new Callback<List<Reaction>>() {
            @Override
            public void onResponse(Call<List<Reaction>> call, Response<List<Reaction>> response) {

                List<Reaction> reactions = response.body();
                for(Reaction reaction : reactions){
                    recyclerView = findViewById(R.id.reaction_list);
                    recyclerView.setLayoutManager(new LinearLayoutManager(UserReactionListActivity.this));
                    recyclerView.setAdapter(new UserReactionAdapter(response.body(), getApplicationContext()));
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                    recyclerView.addItemDecoration(dividerItemDecoration);
                }
            }
            @Override
            public void onFailure(Call<List<Reaction>> call, Throwable t) {
            }
        });

    }

    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserReactionListActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
