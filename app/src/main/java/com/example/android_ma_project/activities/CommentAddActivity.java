package com.example.android_ma_project.activities;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.service.CommentServiceInterface;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentAddActivity extends Activity {

    private int idPost;
    private EditText comment_text_add;
    private Button addComment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_add_activity);

        comment_text_add = findViewById(R.id.comment_text_add);

        addComment = findViewById(R.id.addComment);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idPost = b.getInt("idPost");

        addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addComment();
            }
        });
    }

    private void addComment() {
        String text = comment_text_add.getText().toString().trim();

        if(text.isEmpty()){
            comment_text_add.setError("Enter text");
            comment_text_add.requestFocus();
            return;
        }
        saveComment(text,idPost, MenuUserActivity.idRedditor);
    }
    private void saveComment(String text,Integer idPost,Integer idUser) {

        Comment comment = new Comment(text,idPost,idUser);

        CommentServiceInterface service = MyRetrofit.retrofit.create(CommentServiceInterface.class);
        Call<Comment> call = service.addComment(comment);
        call.enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {

                Comment commentResponse = response.body();

                String text2 = commentResponse.getText();
                Integer idPost2 = commentResponse.getIdPost();
                Integer idUser2 = commentResponse.getIdUser();

                comment.setText(text2);
                comment.setIdPost(idPost2);
                comment.setIdUser(idUser2);

                Toast.makeText(CommentAddActivity.this, "Comment successfully added", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CommentAddActivity.this, UserCommunityListActivity.class);
                startActivity(i);

            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CommentAddActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
