package com.example.android_ma_project.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.RedditorServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommunityActivity extends Activity {

    private int idCommunity;
    private TextView community_name_solo, community_description_solo, community_creation_date_solo, community_rules_solo;
    private ImageView user_avatar_solo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");


        community_name_solo = findViewById(R.id.community_name_solo);
        community_description_solo = findViewById(R.id.community_description_solo);
        community_creation_date_solo = findViewById(R.id.community_creation_date_solo);
        community_rules_solo = findViewById(R.id.community_rules_solo);
    }

    @Override
    public void onResume(){
        super.onResume();
        getUserProfile();
    }

    private void getUserProfile() {

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<Community> call = service.getCommunity(idCommunity);
        call.enqueue(new Callback<Community>() {
            @Override
            public void onResponse(Call<Community> call, Response<Community> response) {

                community_name_solo.setText(response.body().getName());
                community_description_solo.setText(response.body().getDescription());
                community_creation_date_solo.setText(response.body().getCreationDate());
                community_rules_solo.setText(response.body().getRules());
            }

            @Override
            public void onFailure(Call<Community> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CommunityActivity.this, MenuUserActivity.class);
        startActivity(intent);
        finish();
    }
}
