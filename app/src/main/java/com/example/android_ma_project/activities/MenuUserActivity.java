package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.PostAdapter;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuUserActivity extends AppCompatActivity {

    public static int idRedditor;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_list_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idRedditor = b.getInt("idRedditor");
    }

    @Override
    public void onResume() {
        super.onResume();
        getPosts();
    }

    private void getPosts() {

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);

        Call<List<Post>> call = service.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                recyclerView = findViewById(R.id.post_list);
                recyclerView.setLayoutManager(new LinearLayoutManager(MenuUserActivity.this));
                recyclerView.setAdapter(new PostAdapter(response.body(), getApplicationContext()));
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });

    }

    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        getMenuInflater().inflate(R.menu.list, menu);
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ITEM ADD ACTIVITY
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.profileRedirect) {
            Intent intent = new Intent(MenuUserActivity.this, UserProfileActivity.class);
            Bundle b1 = new Bundle();
            b1.putInt("idRedditor", idRedditor);
            intent.putExtras(b1);
            startActivity(intent);
            finish();
        } else if (id == R.id.listButton) {
            Intent intent = new Intent(MenuUserActivity.this, UserCommunityListActivity.class);
            Bundle b1 = new Bundle();
            b1.putInt("idRedditor", idRedditor);
            intent.putExtras(b1);
            startActivity(intent);
            finish();
        } else if (id == R.id.logoutRedirect) {
            Intent intent = new Intent(MenuUserActivity.this, NotLoggedMainCommunityActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}
