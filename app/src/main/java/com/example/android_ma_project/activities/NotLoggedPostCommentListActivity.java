package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.NotLoggedCommentAdapter;
import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotLoggedPostCommentListActivity extends AppCompatActivity {

    private int idPost;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_comment_list_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idPost = b.getInt("idPost");

    }

    @Override
    public void onResume(){
        super.onResume();
        getCommentsOfPost();
    }

    private void getCommentsOfPost() {

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);

        Call<List<Comment>> call = service.getCommentsOfPost(idPost);
        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {

                List<Comment> comments = response.body();
                for(Comment comment : comments){
                    recyclerView = findViewById(R.id.community_post_list_rv);
                    recyclerView.setLayoutManager(new LinearLayoutManager(NotLoggedPostCommentListActivity.this));
                    recyclerView.setAdapter(new NotLoggedCommentAdapter(response.body(), getApplicationContext()));
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                    recyclerView.addItemDecoration(dividerItemDecoration);
                }
            }
            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
            }
        });

    }



    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NotLoggedPostCommentListActivity.this, NotLoggedMainCommunityActivity.class);
        startActivity(intent);
        finish();
    }
}
