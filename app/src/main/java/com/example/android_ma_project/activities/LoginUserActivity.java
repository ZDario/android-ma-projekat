package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.User;
import com.example.android_ma_project.model.UserType;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.UserServiceInterface;

import java.text.ParseException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginUserActivity extends AppCompatActivity {

    private EditText userNameEt, passwordEt;
    private Button SignInButton;
    private TextView SignUpTv;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_user_activity);
        userNameEt = findViewById(R.id.userName );
        passwordEt = findViewById(R.id.password );
        SignInButton = findViewById(R.id.login);
        SignUpTv = findViewById(R.id.signUpTv);



        SignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = userNameEt.getText().toString().trim();
                String password = passwordEt.getText().toString().trim();

                if (TextUtils.isEmpty(userName)) {
                    userNameEt.setError("Enter your username");
                    userNameEt.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(password)) {
                    passwordEt.setError("Enter your password");
                    passwordEt.requestFocus();
                    return;
                }

                loginUser(userName, password);
            }
        });


        //REDIRECT TO USER REGISTER
        SignUpTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginUserActivity.this, RegisterUserActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void loginUser(String userName, String pass){

        User user = new User(userName, pass);

        UserServiceInterface service = MyRetrofit.retrofit.create(UserServiceInterface.class);

        Call<User> call = service.login(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                User userResponse = response.body();
                if (userName.equals(userResponse.getUserName()) && pass.equals(userResponse.getPassword()) && userResponse.getUserType().equals("ADMIN")) {
                    Toast.makeText(LoginUserActivity.this, "Successfully logged", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginUserActivity.this, MenuAdminActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("idAdmin", response.body().getIdUser());
                    intent.putExtras(b);
                    LoginUserActivity.this.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } else if (userName.equals(userResponse.getUserName()) && pass.equals(userResponse.getPassword()) && userResponse.getUserType().equals("REDDITOR")) {
                    Toast.makeText(LoginUserActivity.this, "Successfully logged", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginUserActivity.this, MenuUserActivity.class);
                    Bundle b = new Bundle();
                    b.putInt("idRedditor", response.body().getIdUser());
                    intent.putExtras(b);
                    LoginUserActivity.this.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } else {
                    Toast.makeText(LoginUserActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }

    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginUserActivity.this, NotLoggedMainCommunityActivity.class);
        startActivity(intent);
        finish();
    }
}
