package com.example.android_ma_project.activities;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.List;
import java.util.Objects;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.PostAdapter;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminPostListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_list_activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPosts();
    }

    private void getPosts() {

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);

        Call<List<Post>> call = service.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                recyclerView = findViewById(R.id.post_list);
                recyclerView.setLayoutManager(new LinearLayoutManager(AdminPostListActivity.this));
                recyclerView.setAdapter(new PostAdapter(response.body(), getApplicationContext()));
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AdminPostListActivity.this, MenuAdminActivity.class);
        startActivity(intent);
        finish();
    }
}
