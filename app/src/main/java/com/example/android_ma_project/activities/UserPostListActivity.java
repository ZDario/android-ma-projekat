package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.NotLoggedCommentAdapter;
import com.example.android_ma_project.adapters.NotLoggedPostAdapter;
import com.example.android_ma_project.adapters.UserPostAdapter;
import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPostListActivity extends AppCompatActivity {

    private int idCommunity;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_post_list_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");
    }

    @Override
    public void onResume(){
        super.onResume();
        getPostsOfCommunity();
    }

    private void getPostsOfCommunity() {

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<List<Post>> call = service.getPostsOfCommunity(idCommunity);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                List<Post> posts = response.body();
                for(Post post : posts){
                    recyclerView = findViewById(R.id.community_post_list_rv);
                    recyclerView.setLayoutManager(new LinearLayoutManager(UserPostListActivity.this));
                    recyclerView.setAdapter(new UserPostAdapter(response.body(), getApplicationContext()));
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                    recyclerView.addItemDecoration(dividerItemDecoration);
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
            }
        });

    }

    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserPostListActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }

    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add,menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ...
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.add){
            Intent intent = new Intent(UserPostListActivity.this, PostAddActivityUser.class);
            Bundle b1 = new Bundle();
            b1.putInt("idCommunity", idCommunity);
            intent.putExtras(b1);
            startActivity(intent);
            finish();
        }
        return true;
    }
}