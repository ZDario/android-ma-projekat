package com.example.android_ma_project.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.NotLoggedCommunityAdapter;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotLoggedMainCommunityActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_list_activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        getCommunities();
    }

    private void getCommunities() {

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<List<Community>> call = service.getCommunities();
        call.enqueue(new Callback<List<Community>>() {
            @Override
            public void onResponse(Call<List<Community>> call, Response<List<Community>> response) {
                recyclerView = findViewById(R.id.community_list_rv);
                recyclerView.setLayoutManager(new LinearLayoutManager(NotLoggedMainCommunityActivity.this));
                recyclerView.setAdapter(new NotLoggedCommunityAdapter(response.body(), getApplicationContext()));
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);

            }

            @Override
            public void onFailure(Call<List<Community>> call, Throwable t) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }

    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login,menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ITEM ADD ACTIVITY
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.loginRedirect){
            Intent intent = new Intent(NotLoggedMainCommunityActivity.this, LoginUserActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }
}