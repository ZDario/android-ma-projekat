package com.example.android_ma_project.activities;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.NotLoggedCommunityAdapter;
import com.example.android_ma_project.adapters.UserCommunityAdapter;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCommunityListActivity extends AppCompatActivity {

    public static int idCommunity;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    SensorManager sensorManager;
    Sensor sensor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_list_activity);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        // Register the listener
        sensorManager.registerListener(gyroListener,
                sensor, SensorManager.SENSOR_DELAY_NORMAL);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");
    }

    @Override
    public void onResume() {
        super.onResume();
        getCommunities();
        sensorManager.registerListener(gyroListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void getCommunities() {

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<List<Community>> call = service.getCommunities();
        call.enqueue(new Callback<List<Community>>() {
            @Override
            public void onResponse(Call<List<Community>> call, Response<List<Community>> response) {
                recyclerView = findViewById(R.id.community_list_rv);
                recyclerView.setLayoutManager(new LinearLayoutManager(UserCommunityListActivity.this));
                recyclerView.setAdapter(new UserCommunityAdapter(response.body(), getApplicationContext()));
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);

            }

            @Override
            public void onFailure(Call<List<Community>> call, Throwable t) {

            }
        });

    }
    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add,menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ...
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.add){
            Intent intent = new Intent(UserCommunityListActivity.this, CommunityAddActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

    public void onStop() {
        super.onStop();
        sensorManager.unregisterListener(gyroListener);
    }

    public SensorEventListener gyroListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor sensor, int acc) {
        }

        public void onSensorChanged(SensorEvent event) {
            if(event.values[2] > 1f) { // anticlockwise
                Intent intent = new Intent(UserCommunityListActivity.this, CommunityAddActivity.class);
                startActivity(intent);
                finish();
            } else if(event.values[2] < -0.5f) { // clockwise
                Intent intent = new Intent(UserCommunityListActivity.this, CommunityAddActivity.class);
                startActivity(intent);
                finish();
            }
// https://gist.github.com/sunmeat/de1be188e756e95997bacd9af6a96a99         Link for Gyroscope
// https://code.tutsplus.com/tutorials/android-sensors-in-depth-proximity-and-gyroscope--cms-28084
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserCommunityListActivity.this, MenuUserActivity.class);
        startActivity(intent);
        finish();
    }
}
