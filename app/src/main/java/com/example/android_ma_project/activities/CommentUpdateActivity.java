package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.service.CommentServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.RedditorServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentUpdateActivity extends AppCompatActivity {

    private int idComment;
    private Boolean isDeleted=true;
    private TextView comment_text_update;
    private Button updateComment, deleteComment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_update_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idComment = b.getInt("idComment");

        comment_text_update = findViewById(R.id.comment_text_update);
        updateComment = findViewById(R.id.updateComment);
        deleteComment = findViewById(R.id.deleteComment);

        updateComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editComment();
            }
        });

        deleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteComment(isDeleted);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        getComment();
    }

    private void getComment(){
        CommentServiceInterface service = MyRetrofit.retrofit.create(CommentServiceInterface.class);

        Call<Comment> call = service.getComment(idComment);
        call.enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {

                Comment comment = response.body();
                comment_text_update.setText(response.body().getText());
            }
            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
            }
        });
    }

    private void editComment() {
        String comment_text = comment_text_update.getText().toString().trim();

        if(comment_text.isEmpty()){
            comment_text_update.setError("Enter new comment");
            comment_text_update.requestFocus();
            return;
        }

        saveUpdatedComment(comment_text, isDeleted);
    }

    private void saveUpdatedComment(String comment_text, Boolean isDeleted) {

        Comment comment = new Comment(comment_text, isDeleted);

        CommentServiceInterface service = MyRetrofit.retrofit.create(CommentServiceInterface.class);

        Call<Comment> call = service.editComment(comment, idComment);
        call.enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {

                Comment commentResponse = response.body();

                String text = commentResponse.getText();
                Boolean isDeleted1 = commentResponse.getDeleted();

                comment.setText(text);
                comment.setDeleted(isDeleted1);

                Toast.makeText(CommentUpdateActivity.this, "You have successfully updated your comment", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CommentUpdateActivity.this, UserCommunityListActivity.class);
                CommentUpdateActivity.this.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
            }
        });
    }

    private void deleteComment(Boolean isDeleted) {

        Comment comment = new Comment(isDeleted);
        CommentServiceInterface service = MyRetrofit.retrofit.create(CommentServiceInterface.class);
        Call<Comment> call = service.commentDelete(comment, idComment);
        call.enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                Comment commentResponse = response.body();
                Boolean isDeleted1 = commentResponse.getDeleted();
                comment.setDeleted(isDeleted1);

                Toast.makeText(CommentUpdateActivity.this, "You have successfully deleted comment", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CommentUpdateActivity.this, UserCommunityListActivity.class);
                startActivity(i);
            }
            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CommentUpdateActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
