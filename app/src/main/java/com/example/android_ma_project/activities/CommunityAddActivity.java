package com.example.android_ma_project.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommunityAddActivity extends AppCompatActivity {

    private EditText community_name, community_description, community_rules;
    private Button addCommunity;

    SensorManager sensorManager;
    Sensor proximitySensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_add_activity);

        community_name = findViewById(R.id.community_name_add);
        community_description = findViewById(R.id.community_description_add);
        community_rules = findViewById(R.id.community_rules_add);

        addCommunity = findViewById(R.id.addCommunity);


        //Acquire the Proximity Sensor
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        // Register it, specifying the polling interval in
        // microseconds
        sensorManager.registerListener(proximitySensorListener,
                proximitySensor, 2 * 1000 * 1000);

        addCommunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCommunity();
            }
        });
    }

    private void addCommunity() {
        String name = community_name.getText().toString().trim();
        String description = community_description.getText().toString().trim();
        String rules = community_rules.getText().toString().trim();

        if(name.isEmpty()){
            community_name.setError("Enter name");
            community_name.requestFocus();
            return;
        }

        if(description.isEmpty()){
            community_description.setError("Enter description");
            community_description.requestFocus();
            return;
        }

        if(rules.isEmpty()){
            community_rules.setError("Enter rules");
            community_rules.requestFocus();
            return;
        }

        saveCommunity(name,description,rules);
    }

    private void saveCommunity(String name, String description, String rules) {

        Community community = new Community(name, description, rules);

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);
        Call<Community> call = service.addCommunity(community);
        call.enqueue(new Callback<Community>() {
            @Override
            public void onResponse(Call<Community> call, Response<Community> response) {

                Community communityResponse = response.body();

                String name2 = communityResponse.getName();
                String description2 = communityResponse.getDescription();
                String rules2 = communityResponse.getRules();

                community.setName(name2);
                community.setDescription(description2);
                community.setRules(rules2);

                Toast.makeText(CommunityAddActivity.this, "Community successfully added", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CommunityAddActivity.this, UserCommunityListActivity.class);
                startActivity(i);

            }

            @Override
            public void onFailure(Call<Community> call, Throwable t) {

            }
        });

    }

    //ON BACK BUTTON PRESSED GOES BACK TO COMMUNITY LIST
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CommunityAddActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }

    public void onResume() {
        super.onResume();
        sensorManager.registerListener(proximitySensorListener, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void onStop() {
        super.onStop();
        sensorManager.unregisterListener(proximitySensorListener);
    }

    // Create listener
    SensorEventListener proximitySensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            if(sensorEvent.values[0] < proximitySensor.getMaximumRange()) {
                // Detected something nearby
                Intent intent = new Intent(CommunityAddActivity.this, UserCommunityListActivity.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    };
}

