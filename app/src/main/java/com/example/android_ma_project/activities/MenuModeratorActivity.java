//package com.example.android_ma_project.activities;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//
//import com.example.android_ma_project.R;
//
//public class MenuModeratorActivity extends AppCompatActivity {
//
//    private Button UserList, CommunityUpdateButton;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.menu_moderator_activity);
//
//        UserList = findViewById(R.id.user_list_button);
//        CommunityUpdateButton = findViewById(R.id.communityUpdate);
//
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        UserList.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MenuModeratorActivity.this, UserListActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        CommunityUpdateButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MenuModeratorActivity.this, CommunityUpdateActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//    }
//
//    //CREATES BUTTON ON TOOLBAR
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.logout, menu);
//        return true;
//    }
//
//    //ON TOOLBAR BUTTON PRESSED GOES TO ITEM ADD ACTIVITY
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.logout_button) {
//            Intent intent = new Intent(MenuModeratorActivity.this, LoginUserActivity.class);
//            startActivity(intent);
//            finish();
//        }
//        return true;
//    }
//
//    @Override
//    public void onBackPressed() {
//        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
//    }
//}
