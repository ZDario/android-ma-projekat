package com.example.android_ma_project.activities;

import android.content.Context;
import android.content.Intent;

import java.util.List;
import java.util.Objects;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.NotLoggedPostAdapter;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotLoggedCommunityPostListActivity extends AppCompatActivity {

    private int idCommunity;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    private SensorManager mSensorManager;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_post_list_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");

        //SENSOR EVENT GETTING PARAMETERS
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Objects.requireNonNull(mSensorManager).registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 10f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

    }

    private void getPostsOfCommunity() {

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<List<Post>> call = service.getPostsOfCommunity(idCommunity);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                List<Post> posts = response.body();
                for(Post post : posts){
                        recyclerView = findViewById(R.id.community_post_list_rv);
                        recyclerView.setLayoutManager(new LinearLayoutManager(NotLoggedCommunityPostListActivity.this));
                        recyclerView.setAdapter(new NotLoggedPostAdapter(response.body(), getApplicationContext()));
                        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                        recyclerView.addItemDecoration(dividerItemDecoration);
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
            }
        });
    }

    //SENSOR SHAKE EVENT FINDING IF WE SHAKE PHONE
    private final SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta;
            if (mAccel > 10) {
                linearLayoutManager.setReverseLayout(true);
                linearLayoutManager.setStackFromEnd(true);
                recyclerView.setLayoutManager(linearLayoutManager);
            } else if(mAccel > 7){
                linearLayoutManager.setReverseLayout(false);
                linearLayoutManager.setStackFromEnd(false);
                recyclerView.setLayoutManager(linearLayoutManager);
            }
        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        getPostsOfCommunity();
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }



    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NotLoggedCommunityPostListActivity.this, NotLoggedMainCommunityActivity.class);
        startActivity(intent);
        finish();
    }
}
