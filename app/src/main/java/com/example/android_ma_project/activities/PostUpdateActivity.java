package com.example.android_ma_project.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;
import com.example.android_ma_project.service.RedditorServiceInterface;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostUpdateActivity extends AppCompatActivity {

    private int idPost;
    private EditText post_title_update, post_text_update, post_image_path_update;
    private Button updatePostButton, deletePostButton;
    private String sImage;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_update_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idPost = b.getInt("idPost");

        post_title_update = findViewById(R.id.post_title_update );
        post_text_update = findViewById(R.id.post_text_update );
        post_image_path_update = findViewById(R.id.post_image_path_update);

        updatePostButton = findViewById(R.id.updatePostButton);
        deletePostButton = findViewById(R.id.deletePostButton);

        updatePostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPost();
            }
        });
        deletePostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePost();
            }
        });
        post_image_path_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check condition
                if (ContextCompat.checkSelfPermission(PostUpdateActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {
                    // when permission is nor granted
                    // request permission
                    ActivityCompat.requestPermissions(PostUpdateActivity.this
                            , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);

                }
                else
                {
                    // when permission
                    // is granted
                    // create method
                    selectImage();
                }
            }
        });
    }

    private void selectImage() {
        // clear previous data
        post_image_path_update.setText("");
        // Initialize intent
        Intent intent=new Intent(Intent.ACTION_PICK);
        // set type
        intent.setType("image/*");
        // start activity result
        startActivityForResult(Intent.createChooser(intent,"Select Image"),100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull  int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // check condition
        if (requestCode==100 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
        {
            // when permission
            // is granted
            // call method
            selectImage();
        }
        else
        {
            // when permission is denied
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check condition
        if (requestCode==100 && resultCode==RESULT_OK && data!=null)
        {
            // when result is ok
            // initialize uri
            Uri uri=data.getData();
            // Initialize bitmap
            try {
                Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                // initialize byte stream
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                // compress Bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
                // Initialize byte array
                byte[] bytes=stream.toByteArray();
                // get base64 encoded string
                sImage= Base64.encodeToString(bytes,Base64.DEFAULT);
                // set encoded text on textview
                post_image_path_update.setText(sImage);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getPost();
    }

    private void getPost(){
        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);
        Call<Post> call = service.getPost(idPost);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Post post = response.body();
                post_title_update.setText(response.body().getTitle());
                post_text_update.setText(response.body().getText());
                post_image_path_update.setText(sImage);
            }
            @Override
            public void onFailure(Call<Post> call, Throwable t) {
            }
        });
    }

    private void editPost() {
        String post_title = post_title_update.getText().toString().trim();
        String post_text = post_text_update.getText().toString().trim();
        String post_image_path = post_image_path_update.getText().toString().trim();

        if(post_title.isEmpty()){
            post_title_update.setError("Enter new title");
            post_title_update.requestFocus();
            return;
        }
        else if(post_text.isEmpty()){
            post_text_update.setError("Enter new text");
            post_text_update.requestFocus();
            return;
        }
        else if(post_image_path.isEmpty()){
            post_image_path_update.setError("Enter new image");
            post_image_path_update.requestFocus();
            return;
        }

        saveUpdatedPost(post_title, post_text, post_image_path);
    }

    private void saveUpdatedPost(String post_title, String post_text, String post_image_path) {

        Post post = new Post(post_title, post_text, post_image_path);

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);

        Call<Post> call = service.editPost(post, idPost);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                Post postResponse = response.body();

                String title = postResponse.getTitle();
                String text = postResponse.getText();
                String image_path = postResponse.getImagePath();

                post.setTitle(title);
                post.setText(text);
                post.setImagePath(image_path);

                Toast.makeText(PostUpdateActivity.this, "You have successfully updated your post", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(PostUpdateActivity.this, UserCommunityListActivity.class);
                PostUpdateActivity.this.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            @Override
            public void onFailure(Call<Post> call, Throwable t) {
            }
        });
    }

    private void deletePost() {

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);

        Call<Void> call = service.deletePost(idPost);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(PostUpdateActivity.this, "Post has been deleted successfully", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(PostUpdateActivity.this, UserCommunityListActivity.class);
                startActivity(i);
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });

    }




    //ON BACK BUTTON PRESSED GOES BACK TO POST LIST
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PostUpdateActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
