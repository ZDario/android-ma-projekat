package com.example.android_ma_project.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Community;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.service.CommunityServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommunityUpdateActivity extends AppCompatActivity {

    private int idCommunity;
    private EditText community_description_update, community_rules_update;
    private Button updateCommunityButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_update_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");

        community_description_update = findViewById(R.id.community_description_update );
        community_rules_update = findViewById(R.id.community_rules_update);

        updateCommunityButton = findViewById(R.id.updateCommunityButton);

        updateCommunityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editCommunity();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        getCommunity();
    }

    private void getCommunity(){
        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);
        Call<Community> call = service.getCommunity(idCommunity);
        call.enqueue(new Callback<Community>() {
            @Override
            public void onResponse(Call<Community> call, Response<Community> response) {
                Community community = response.body();
                community_description_update.setText(response.body().getDescription());
                community_rules_update.setText(response.body().getRules());
            }
            @Override
            public void onFailure(Call<Community> call, Throwable t) {
            }
        });
    }

    private void editCommunity() {
        String community_description = community_description_update.getText().toString().trim();
        String community_rules = community_rules_update.getText().toString().trim();

        if(community_rules.isEmpty()){
            community_rules_update.setError("Enter new rules");
            community_rules_update.requestFocus();
            return;
        }
        else if(community_description.isEmpty()){
            community_description_update.setError("Enter new description");
            community_description_update.requestFocus();
            return;
        }

        saveUpdatedCommunity(community_description, community_rules);
    }

    private void saveUpdatedCommunity(String community_description, String community_rules) {

        Community community = new Community(community_description, community_rules);

        CommunityServiceInterface service = MyRetrofit.retrofit.create(CommunityServiceInterface.class);

        Call<Community> call = service.editCommunity(community, idCommunity);
        call.enqueue(new Callback<Community>() {
            @Override
            public void onResponse(Call<Community> call, Response<Community> response) {

                Community communityResponse = response.body();

                String description = communityResponse.getDescription();
                String rules = communityResponse.getRules();

                community.setDescription(description);
                community.setRules(rules);

                Toast.makeText(CommunityUpdateActivity.this, "You have successfully updated your community", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CommunityUpdateActivity.this, UserCommunityListActivity.class);
                CommunityUpdateActivity.this.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            @Override
            public void onFailure(Call<Community> call, Throwable t) {
            }
        });
    }

    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CommunityUpdateActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
