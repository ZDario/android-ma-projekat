package com.example.android_ma_project.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.RedditorServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends Activity {

    private int idRedditor;
    private TextView user_userName_solo, user_email_solo, user_registration_date_solo, change_Profile_Tv;
    private ImageView user_avatar_solo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idRedditor = b.getInt("idRedditor");


        user_userName_solo = findViewById(R.id.user_userName_solo);
        user_email_solo = findViewById(R.id.user_email_solo);
        user_avatar_solo = findViewById(R.id.user_avatar_solo);
        user_registration_date_solo = findViewById(R.id.user_registration_date_solo);
        change_Profile_Tv = findViewById(R.id.change_Profile_Tv);

        //REDIRECT TO USER UPDATE
        change_Profile_Tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, UserProfileUpdateActivity.class);
                Bundle b1 = new Bundle();
                b1.putInt("idRedditor", idRedditor);
                intent.putExtras(b1);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        getUserProfile();
    }

    private void getUserProfile() {

        RedditorServiceInterface service = MyRetrofit.retrofit.create(RedditorServiceInterface.class);

        Call<Redditor> call = service.getRedditor(MenuUserActivity.idRedditor);
        call.enqueue(new Callback<Redditor>() {
            @Override
            public void onResponse(Call<Redditor> call, Response<Redditor> response) {

                user_userName_solo.setText(response.body().getUserNameRedditor());
                user_email_solo.setText(response.body().getEmailRedditor());
//                user_avatar_solo.setText(response.body().getAvatarRedditor());
                user_registration_date_solo.setText(response.body().getRegistrationDateRedditor());
                String avatar = response.body().getAvatarRedditor().toString();


                byte[] decodedString = Base64.decode(avatar, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                user_avatar_solo.setImageBitmap(decodedByte);
            }

            @Override
            public void onFailure(Call<Redditor> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserProfileActivity.this, MenuUserActivity.class);
        startActivity(intent);
        finish();
    }
}
