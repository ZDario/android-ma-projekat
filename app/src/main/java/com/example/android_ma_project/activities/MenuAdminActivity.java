package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_ma_project.R;

public class MenuAdminActivity extends AppCompatActivity {

    private Button PostListButton, CommunityListButton, CommentListButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_admin_activity);

        PostListButton = findViewById(R.id.postListAdmin);
        CommunityListButton = findViewById(R.id.communityListAdmin);
        CommentListButton = findViewById(R.id.commentListAdmin);

        PostListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuAdminActivity.this, AdminPostListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        CommunityListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuAdminActivity.this, AdminCommunityListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        CommentListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuAdminActivity.this, AdminCommentListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }


    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO LOGIN ACTIVITY
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logoutRedirect) {
            Intent intent = new Intent(MenuAdminActivity.this, NotLoggedMainCommunityActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}
