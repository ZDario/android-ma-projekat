package com.example.android_ma_project.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_ma_project.R;
import com.example.android_ma_project.adapters.CommentAdapter;
import com.example.android_ma_project.model.Comment;
import com.example.android_ma_project.service.CommentServiceInterface;
import com.example.android_ma_project.service.MyRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminCommentListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_list_activity);

    }

    @Override
    public void onResume() {
        super.onResume();
        getComments();
    }

    private void getComments() {

        CommentServiceInterface service = MyRetrofit.retrofit.create(CommentServiceInterface.class);

        Call<List<Comment>> call = service.getComments();
        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                recyclerView = findViewById(R.id.comment_list_rv);
                recyclerView.setLayoutManager(new LinearLayoutManager(AdminCommentListActivity.this));
                recyclerView.setAdapter(new CommentAdapter(response.body(), getApplicationContext()));
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
                recyclerView.addItemDecoration(dividerItemDecoration);
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {

            }
        });

    }

    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ITEM ADD ACTIVITY
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add) {
            Intent intent = new Intent(AdminCommentListActivity.this, CommentAddActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AdminCommentListActivity.this, MenuAdminActivity.class);
        startActivity(intent);
        finish();
    }
}
