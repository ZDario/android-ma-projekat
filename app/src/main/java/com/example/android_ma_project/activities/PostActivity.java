package com.example.android_ma_project.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.model.Reaction;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;
import com.example.android_ma_project.service.ReactionServiceInterface;
import com.example.android_ma_project.service.RedditorServiceInterface;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends Activity {

    private int idPost;
    private TextView post_title, post_text, post_creation_date, post_id_community, post_id_user;
    private ImageView upVote, downVote, post_image_path;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_activity);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idPost = b.getInt("idPost");

        post_title = findViewById(R.id.post_title_solo );
        post_text = findViewById(R.id.post_text_solo );
        post_creation_date = findViewById(R.id.post_creation_date_solo );
        post_image_path = findViewById(R.id.post_image_path_solo);
        post_id_community = findViewById(R.id.post_id_community_solo);
        post_id_user = findViewById(R.id.post_id_user_solo);

        upVote = findViewById(R.id.upVote);
        downVote = findViewById(R.id.downVote);

        upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String up_vote = "UPVOTE";
                saveReaction(up_vote, idPost, MenuUserActivity.idRedditor);
                upVote.setEnabled(false);
                downVote.setEnabled(false);
            }
        });
        downVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String down_vote = "DOWNVOTE";
                saveReaction(down_vote, idPost, MenuUserActivity.idRedditor);
                upVote.setEnabled(false);
                downVote.setEnabled(false);
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        getPost();
    }

    private void getPost() {
        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);
        Call<Post> call = service.getPost(idPost);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                post_title.setText(response.body().getTitle());
                post_text.setText(response.body().getText());
                post_creation_date.setText(response.body().getCreationDate());
                //post_image_path.setText(response.body().getImagePath());
                post_id_community.setText(response.body().getName());
                post_id_user.setText(response.body().getUserName());

                String image_path = response.body().getImagePath().toString();

                byte[] decodedString = Base64.decode(image_path, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                post_image_path.setImageBitmap(decodedByte);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }

    private void saveReaction(String reactionType, Integer idPost, Integer idUser) {

        Reaction reaction = new Reaction(reactionType, idPost, idUser);

        ReactionServiceInterface service = MyRetrofit.retrofit.create(ReactionServiceInterface.class);
        Call<Reaction> call = service.addReaction(reaction);
        call.enqueue(new Callback<Reaction>() {
            @Override
            public void onResponse(Call<Reaction> call, Response<Reaction> response) {

                Reaction reactionResponse = response.body();

                String reactionType2 = reactionResponse.getReactionType();
                Integer idPost2 = reactionResponse.getIdPost();
                Integer idUser2 = reactionResponse.getIdUser();

                reaction.setReactionType(reactionType2);
                reaction.setIdPost(idPost2);
                reaction.setIdUser(idUser2);

                Toast.makeText(PostActivity.this, "You have reacted this post", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<Reaction> call, Throwable t) {
            }
        });
    }

    //ON BACK BUTTON PRESSED GOES BACK TO POST LIST
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PostActivity.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
