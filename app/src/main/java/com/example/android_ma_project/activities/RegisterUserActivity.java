package com.example.android_ma_project.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.model.Redditor;
import com.example.android_ma_project.model.User;
import com.example.android_ma_project.model.UserType;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;
import com.example.android_ma_project.service.RedditorServiceInterface;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUserActivity extends AppCompatActivity {

    EditText userNameEt, passwordEt, emailEt, avatarEt;
    Button SignUpButton;
    TextView goToLoginPage;
    String sImage;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_user_activity);


        userNameEt = findViewById(R.id.userName);
        passwordEt = findViewById(R.id.password );
        emailEt = findViewById(R.id.email );
        avatarEt = findViewById(R.id.avatar );

        SignUpButton = findViewById(R.id.register);
        goToLoginPage = findViewById(R.id.goToLoginPage);

        SignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });
        goToLoginPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterUserActivity.this, LoginUserActivity.class);
                startActivity(intent);
                finish();
            }
        });



        avatarEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check condition
                if (ContextCompat.checkSelfPermission(RegisterUserActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {
                    // when permission is nor granted
                    // request permission
                    ActivityCompat.requestPermissions(RegisterUserActivity.this
                            , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);

                }
                else
                {
                    // when permission
                    // is granted
                    // create method
                    selectImage();
                }
            }
        });
    }

    private void selectImage() {
        // clear previous data
        avatarEt.setText("");
        // Initialize intent
        Intent intent=new Intent(Intent.ACTION_PICK);
        // set type
        intent.setType("image/*");
        // start activity result
        startActivityForResult(Intent.createChooser(intent,"Select Image"),100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull  int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // check condition
        if (requestCode==100 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
        {
            // when permission
            // is granted
            // call method
            selectImage();
        }
        else
        {
            // when permission is denied
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check condition
        if (requestCode==100 && resultCode==RESULT_OK && data!=null)
        {
            // when result is ok
            // initialize uri
            Uri uri=data.getData();
            // Initialize bitmap
            try {
                Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                // initialize byte stream
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                // compress Bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
                // Initialize byte array
                byte[] bytes=stream.toByteArray();
                // get base64 encoded string
                sImage= Base64.encodeToString(bytes,Base64.DEFAULT);
                // set encoded text on textview
                avatarEt.setText(sImage);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }




    private void addUser() {
        String userName = userNameEt.getText().toString().trim();
        String password = passwordEt.getText().toString().trim();
        String email = emailEt.getText().toString().trim();
        String avatar = avatarEt.getText().toString().trim();

        if(userName.isEmpty()){
            userNameEt.setError("Enter userName");
            userNameEt.requestFocus();
            return;
        }else if(password.isEmpty()){
            passwordEt.setError("Enter password");
            passwordEt.requestFocus();
            return;
        }else if(email.isEmpty()){
            emailEt.setError("Enter email");
            emailEt.requestFocus();
            return;
        }else if(avatar.isEmpty()){
            avatarEt.setError("Enter email");
            avatarEt.requestFocus();
            return;
        }else if (!isValidEmail(email)) {
            emailEt.setError("Invalid Email");
            return;
        }


        saveUser(userName,password,email,avatar);
    }
    private void saveUser(String userName, String password, String email, String avatar) {

        Redditor redditor = new Redditor(userName, password, email, avatar);

        RedditorServiceInterface service = MyRetrofit.retrofit.create(RedditorServiceInterface.class);
        Call<Redditor> call = service.addRedditor(redditor);
        call.enqueue(new Callback<Redditor>() {
            @Override
            public void onResponse(Call<Redditor> call, Response<Redditor> response) {

                Redditor userResponse = response.body();

                String userName2 = userResponse.getUserNameRedditor();
                String password2 = userResponse.getPasswordRedditor();
                String email2 = userResponse.getEmailRedditor();
                String avatar2 = userResponse.getAvatarRedditor();

                redditor.setUserNameRedditor(userName2);
                redditor.setPasswordRedditor(password2);
                redditor.setEmailRedditor(email2);
                redditor.setAvatarRedditor(avatar2);

                Toast.makeText(RegisterUserActivity.this, "User successfully added", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(RegisterUserActivity.this, LoginUserActivity.class);
                startActivity(i);
            }
            @Override
            public void onFailure(Call<Redditor> call, Throwable t) {
            }
        });
    }

    private Boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(RegisterUserActivity.this, LoginUserActivity.class);
        startActivity(intent);
        finish();
    }
}
