package com.example.android_ma_project.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.android_ma_project.R;
import com.example.android_ma_project.model.Post;
import com.example.android_ma_project.service.MyRetrofit;
import com.example.android_ma_project.service.PostServiceInterface;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostAddActivityUser extends AppCompatActivity {

    private int idCommunity;
    private EditText post_title_add, post_text_add, post_image_path_add;
    private Button addPost;
    private String sImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_add_activity);

        post_title_add = findViewById(R.id.post_title_add);
        post_text_add = findViewById(R.id.post_text_add);
        post_image_path_add = findViewById(R.id.post_image_path_add);
        addPost = findViewById(R.id.addPost);

        Bundle b = getIntent().getExtras();
        if(b != null)
            idCommunity = b.getInt("idCommunity");


        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPost();
            }
        });
        post_image_path_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // check condition
                if (ContextCompat.checkSelfPermission(PostAddActivityUser.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                {
                    // when permission is nor granted
                    // request permission
                    ActivityCompat.requestPermissions(PostAddActivityUser.this
                            , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},100);

                }
                else
                {
                    // when permission
                    // is granted
                    // create method
                    selectImage();
                }
            }
        });
    }

    private void selectImage() {
        // clear previous data
        post_image_path_add.setText("");
        // Initialize intent
        Intent intent=new Intent(Intent.ACTION_PICK);
        // set type
        intent.setType("image/*");
        // start activity result
        startActivityForResult(Intent.createChooser(intent,"Select Image"),100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull  int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // check condition
        if (requestCode==100 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
        {
            // when permission
            // is granted
            // call method
            selectImage();
        }
        else
        {
            // when permission is denied
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check condition
        if (requestCode==100 && resultCode==RESULT_OK && data!=null)
        {
            // when result is ok
            // initialize uri
            Uri uri=data.getData();
            // Initialize bitmap
            try {
                Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),uri);
                // initialize byte stream
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                // compress Bitmap
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
                // Initialize byte array
                byte[] bytes=stream.toByteArray();
                // get base64 encoded string
                sImage= Base64.encodeToString(bytes,Base64.DEFAULT);
                // set encoded text on textview
                post_image_path_add.setText(sImage);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addPost() {
        String title = post_title_add.getText().toString().trim();
        String text = post_text_add.getText().toString().trim();
        String image_path = post_image_path_add.getText().toString().trim();

        if(title.isEmpty()){
            post_title_add.setError("Enter name");
            post_title_add.requestFocus();
            return;
        }else if(text.isEmpty()){
            post_text_add.setError("Enter description");
            post_text_add.requestFocus();
            return;
        }else if(image_path.isEmpty()){
            post_image_path_add.setError("Enter rules");
            post_image_path_add.requestFocus();
            return;
        }
        savePost(title,text,image_path,idCommunity, MenuUserActivity.idRedditor);
    }
    private void savePost(String title, String text, String image_path, Integer idCommunity, Integer idUser) {

        Post post = new Post(title, text, image_path, idCommunity, idUser);

        PostServiceInterface service = MyRetrofit.retrofit.create(PostServiceInterface.class);
        Call<Post> call = service.addPost(post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                Post postResponse = response.body();

                String title2 = postResponse.getTitle();
                String text2 = postResponse.getText();
                String imagePath2 = postResponse.getImagePath();
                Integer idCommunity2 = postResponse.getIdCommunity();
                Integer idUser2 = postResponse.getIdUser();

                post.setTitle(title2);
                post.setText(text2);
                post.setImagePath(imagePath2);
                post.setIdCommunity(idCommunity2);
                post.setIdUser(idUser2);

                Toast.makeText(PostAddActivityUser.this, "Post successfully added", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(PostAddActivityUser.this, UserCommunityListActivity.class);
                startActivity(i);
            }
            @Override
            public void onFailure(Call<Post> call, Throwable t) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PostAddActivityUser.this, UserCommunityListActivity.class);
        startActivity(intent);
        finish();
    }
}
