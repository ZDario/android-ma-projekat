//package com.example.android_ma_project.activities;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.hardware.SensorEventListener;
//import android.os.Bundle;
//import android.util.Base64;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.example.android_ma_project.R;
//import com.example.android_ma_project.adapters.PostAdapter;
//import com.example.android_ma_project.adapters.UserAdapter;
//import com.example.android_ma_project.dao.DatabaseHelper;
//import com.example.android_ma_project.model.Post;
//import com.example.android_ma_project.model.User;
//
//import java.util.ArrayList;
//
//public class UserListActivity extends AppCompatActivity {
//
//    DatabaseHelper databaseHelper;
//    TextView user_id_single, user_userName_single, user_password_single, user_email_single, user_registration_date_single, user_is_banned_single, user_user_type_single;
//    ListView listView;
//    ImageView user_avatar_single;
//    ArrayList<User> arrayList;
//    UserAdapter userAdapter;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.user_list_activity);
//        databaseHelper = new DatabaseHelper(this);
//
//        user_id_single = (TextView) findViewById(R.id.user_id_single);
//        user_userName_single = (TextView) findViewById(R.id.user_userName_single);
//        user_password_single = (TextView) findViewById(R.id.user_password_single);
//        user_email_single = (TextView) findViewById(R.id.user_email_single);
//        user_avatar_single = (ImageView) findViewById(R.id.user_avatar_single);
//        user_registration_date_single = (TextView) findViewById(R.id.user_registration_date_single);
//        user_is_banned_single = (TextView) findViewById(R.id.user_is_banned_single);
//        user_user_type_single = (TextView) findViewById(R.id.user_user_type_single);
//
//        listView = (ListView) findViewById(R.id.user_list);
//        arrayList = new ArrayList<>();
//
//        loadDataInListView();
//    }
//    //LOADS DATA AND REFRESH
//    private void loadDataInListView() {
//        arrayList = databaseHelper.getAllUsers();
//        userAdapter = new UserAdapter(this,arrayList);
//        listView.setAdapter(userAdapter);
//        userAdapter.notifyDataSetChanged();
//    }
//
//    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(UserListActivity.this, MenuModeratorActivity.class);
//        startActivity(intent);
//        finish();
//    }
//}
